# -*- coding: utf-8 -*-

from model_helper import *

class Links(Base):
	__tablename__ = "documents_links"

	id = Column(Integer, primary_key = True)
	url = Column(String(1024), nullable = False)
	created_at = Column(DateTime)
	document = relationship('Documents')
	document_id = Column(
		Integer, 
		ForeignKey('Documents.id'), 
		nullable = True
		)

